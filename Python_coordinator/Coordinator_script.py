from digi.xbee.devices import ZigBeeDevice,RemoteZigBeeDevice,XBee64BitAddress,NetworkEventType
import asyncio
import socketio
from firebase_admin import db, credentials,initialize_app
from firebase_admin import exceptions as firebase_exceptions
from datetime import datetime
import google.auth.exceptions
from serial import serialutil
#PORT = "/dev/ttyUSB0" #raspberry low left usb port
PORT = "COM12"
BAUD_RATE = 9600
device_timeouts_org = dict()
device_timeouts = dict()

coordinator = ZigBeeDevice(PORT, BAUD_RATE)
try:
    coordinator.open()
except serialutil.SerialException as e:
    print(e)
xbee_network = coordinator.get_network()
sio = socketio.AsyncClient(reconnection_delay=3)

@sio.event
async def connect():
    print('--connection established--')
    #Sends a list of the network devices to backend
    addresses = []
    for device in xbee_network.get_devices():
        addresses.append(str(device.get_64bit_addr()))
    await sio.emit('python', addresses)

@sio.event
async def toggle_led(mac):
    print('Toggle LED on:', mac)
    device = RemoteZigBeeDevice(coordinator, XBee64BitAddress.from_hex_string(mac))
    coordinator.send_data_async(device, "toggle_led")

@sio.event
async def disconnect():
    print('--connection lost--')

#Used only for initial connection
async def socket_connect():
    while not sio.connected:
        try:
            await sio.connect('http://localhost:8080')
        except socketio.exceptions.ConnectionError:
            print("Connection error - Retrying in 10 seconds")
            await asyncio.sleep(10)
            continue

#Timer for all the device timeouts
async def device_timeout_loop():
    while True:
        for key in device_timeouts:
            if device_timeouts[key] == 0:
                _device = xbee_network.get_device_by_64(XBee64BitAddress.from_hex_string(key))
                xbee_network.remove_device(_device)
                device_timeouts[key] = -1
            elif device_timeouts[key] > 0:
                device_timeouts[key] -= 1
        await asyncio.sleep(1)

#database
cred_object = credentials.Certificate('firebase-auth.json')
default_app = initialize_app(cred_object, {'databaseURL':'https://fir-project-dd6e1-default-rtdb.europe-west1.firebasedatabase.app'})
ref_new = db.reference("new")
ref_legacy = db.reference("legacy")
ref_legacy_entries = db.reference("legacy_entries/MAC")
#TODO: Add new measurement units to the DB
#Should be checked for when a router sends a init packet

def cb_data_received(xbee_message):
    address = str(xbee_message.remote_device.get_64bit_addr())
    data = xbee_message.data.decode("utf8")
    print("Received data from %s: %s" % (address, data))
    try:
        seperated_data = data.split(",")
        if device_timeouts[address] == -1: #If the device had timed out, but is now sending data agian
            _device = xbee_network.get_device_by_64(XBee64BitAddress.from_hex_string(address))
            xbee_network.add_remote(_device)
        elif seperated_data[0] == "INIT":
            for i in range(len(seperated_data)):
                if seperated_data[i] == "timeout": #Saves the timeout from the INIT message
                    device_timeouts_org[address] = int(seperated_data[i+1])
                    device_timeouts[address] = int(seperated_data[i+1])
            return
        elif device_timeouts[address] == -2: #If the device hasn't sent an init message yet
            return
        device_timeouts[address] = device_timeouts_org[address] #Resets device timeout
        ref_obj = ref_new.get()
        if ref_obj:
            for key, data in ref_new.get().items():
                if data["MAC"] == address and data["unit"] == seperated_data[0]:
                    db.reference("new/"+key).delete() #Deletes data from DB ref if an entry from the same address and unit already exists
                    ref_legacy.push(data) #Pushes the data to the legacy DB reference
        #Uploads the new data to the DB
        ref_new.push({"MAC":address,"value":seperated_data[1],"unit":seperated_data[0],"timestamp":datetime.timestamp(datetime.now())})
    except firebase_exceptions.NotFoundError:
        print("Database not found")
    except google.auth.exceptions.TransportError:
        print("Possible error: No internet connection")
    except KeyError: #Occours if the python script doesn't know about the device, but it is sending data
        device_timeouts[address] = -2
        device = RemoteZigBeeDevice(coordinator, XBee64BitAddress.from_hex_string(address))
        coordinator.send_data_async(device, "send_init") #Requests a init message from the device
        return
coordinator.add_data_received_callback(cb_data_received)

#The argument 'main_loop' is a reference to the async event loop.
#It's needed because the callback functions are synchronous and they need to be able to call async methods.
#That is also why the callback functions are IN the 'main' function.
async def main(main_loop):     
    #Is called when a NEW Xbee is discovered or an existing one is removed.
    #Emits the appropiate event to the webserver.
    def cb_network_modified(event_type, reason, node):
        mac = str(node.get_64bit_addr()) # Gets the MAC address of the newly discovered device
        nonlocal main_loop #A reference to the event loop - sio.emit() is an coroutine/async.
        if sio.connected: #Only emit if Socket.IO has a connection established
            if event_type == NetworkEventType.ADD:
                main_loop.create_task(sio.emit('add_device', mac))
                if not ref_legacy_entries.child(mac).get():
                    ref_legacy_entries.set({ mac: True }) #Adds a DB entry if it doesn't exists
            elif event_type == NetworkEventType.DEL:
                main_loop.create_task(sio.emit('remove_device', mac))
            
        print("  >>>> Network event:")
        print("         Type: %s (%d)" % (event_type.description, event_type.code))
        print("         Reason: %s (%d)" % (reason.description, reason.code))
        print("         Node: %s" % node)
    xbee_network.add_network_modified_callback(cb_network_modified)#Registers the callback function
    
    await socket_connect()
    asyncio.create_task(device_timeout_loop())
    await sio.wait()
try:
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main(loop)) #Note: Passes the async event loop to the 'main' function
    loop.close()
finally:
    print("---------EXITING---------")