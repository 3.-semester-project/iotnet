/*
 * Zigbee_protocol.c
 *
 *  Created on: Sep 28, 2022
 *      Author: Mikkel Svensson
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Zigbee_protocol.h"

XBee *XBeePointerArray[2]; //The amount of possible XBee instances
uint8_t PointerArrayIndex = 0; 	//Counts the number of XBee instances.

//Used for getting the correct XBee pointer in HAL callback events
XBee* _GetXBeePointer(UART_HandleTypeDef* huart, TIM_HandleTypeDef* htim)
{
	for (int i = 0; i < sizeof(XBeePointerArray); i++)
	{
		if (huart != NULL && XBeePointerArray[i]->UART->Instance == huart->Instance)
		{
			return XBeePointerArray[i];
		}
		else if (htim != NULL && XBeePointerArray[i]->Timer.Timer->Instance == htim->Instance)
		{
			return XBeePointerArray[i];
		}
	}
	return NULL;
}

//Is a HAL UART callback registered function.
//HAL_UART_RegisterRxEventCallback
void XBee_ReceivedData(UART_HandleTypeDef* huart, uint16_t Size)
{
	XBee* xbee;
	if ((xbee = _GetXBeePointer(huart, NULL)) != NULL)
	{
		uint8_t* Buf = xbee->RxBuffer;
		HAL_UARTEx_ReceiveToIdle_IT(xbee->UART, Buf, XBEE_BUFFER_SIZE);

/*-----------------------INITILIZATION STATE-----------------------
 * 1. 	INIT_START:	When an OK response has been received from a '+++' command it continues to initialization step 2 (INIT_AP)
 *
 * 2.1.	INIT_AP:	If the response from the 'AP' command is 0 (Meaning the XBee is in Transparent Mode)
 * 					then configure the XBee module and afterwards go to initialization step 3 (INIT_AI)
 * 2.2.	INIT_AP:	If the response from the 'AP' command is above 0 (Meaning the XBee is in either of the API Modes)
 * 					then go to initialization step 3 (INIT_AI)
 *
 * 3.1	INIT_AI:	When the response from the 'AI' command is 0 (Meaning the XBee has joined a network)
 * 					then go to initialization step 4 (INIT_ACK)
 * 3.2	INIT_AI:	When the response from the 'AI' command is 22 (Meaning the XBee couldn't find a valid network)
 * 					then configure the XBee module and afterwards retry initialization step 3 (INIT_AI)
 * 3.3	INIT_AI:	When the response from the 'AI' command is anything else then retry initialization step 3 (INIT_AI)
 *
 * 4.	INIT_ACK:	If an OK response from the 'CN' command has been received end the initialization process
*/
		if (xbee->State == XBEE_STATE_INIT)
		{
			if (xbee->InitState == INIT_START)
			{
				if (strstr((const char*)Buf, INIT_RESPONSE_OK))
				{
					xbee->InitState = INIT_AP;
					HAL_TIM_Base_Stop_IT(xbee->Timer.Timer);
					HAL_UART_Transmit_IT(xbee->UART, XBEE_INIT_COMMANDS[xbee->InitState], 5);
				}
			}
			else if (xbee->InitState == INIT_AP)
			{
				if (strstr((const char*)Buf, INIT_RESPONSE_0))
				{
					XBee_Configure(xbee);
				}
				else
				{
					xbee->InitState = INIT_AI;
					HAL_UART_Transmit_IT(xbee->UART, XBEE_INIT_COMMANDS[xbee->InitState], 5);
				}
			}
			else if (xbee->InitState == INIT_AI)
			{
				if (strstr((const char*)Buf, INIT_RESPONSE_0))
				{
					xbee->InitState = INIT_ACK;
					HAL_UART_Transmit_IT(xbee->UART, XBEE_EXIT_ATC_MODE, 5);
				}
				else if(strstr((const char*)Buf, INIT_RESPONSE_22))
				{
					XBee_Configure(xbee);
				}
				else
				{
					HAL_TIM_Base_Start_IT(xbee->Timer.Timer);
				}
			}
			else if (xbee->InitState == INIT_ACK)
			{
				if (strstr((const char*)Buf, INIT_RESPONSE_OK))
				{
					xbee->InitState = INIT_DONE;
					xbee->State = XBEE_STATE_READY;
					XBee_InitCallback(xbee);
				}
			}
		}
/*-----------------------CONFIGURATION STATE-----------------------
 * If an OK response is received AND there is more commands to be sent then send the next command.
 * If an OK response is received AND there is no more commands to be sent then go to initialization step 3 (INIT_AI)
 */
		else if (xbee->State == XBEE_STATE_CONFIG)
		{
			if (strstr((const char*)Buf, INIT_RESPONSE_OK))
			{
				xbee->ConfigState ++;
			}
			if(xbee->ConfigState < XBEE_CONFIG_COMMANDS_SIZE)
			{
				HAL_UART_Transmit_IT(xbee->UART, XBEE_CONFIG_COMMANDS[xbee->ConfigState], strlen((char*)XBEE_CONFIG_COMMANDS[xbee->ConfigState]));
			}
			else if(xbee->ConfigState == XBEE_CONFIG_COMMANDS_SIZE)
			{
				xbee->State = XBEE_STATE_INIT;
				xbee->InitState = INIT_AI;
				HAL_UART_Transmit_IT(xbee->UART, XBEE_INIT_COMMANDS[INIT_AI], 5);
			}
		}
/*-----------------------READY STATE-----------------------
 * If the first byte of the XBee's RX buffer is 0x7E (Start Delimiter)
 * then decode the data excluding the start delimiter, MSB, LSB and checksum.
 */
		else if (xbee->State == XBEE_STATE_READY || XBEE_STATE_BUSY)
		{
			if (Buf[0] == XBEE_START_DELIMITER)
			{
				XBee_DecodeData(Buf + XBEE_FRAME_OFFSET, Size-4, xbee);
			}
		}
	}
}

//Is a HAL UART callback registered function.
//HAL_UART_RegisterCallback
//HAL_UART_TX_COMPLETE_CB_ID
void XBee_TransmittedData(UART_HandleTypeDef* huart)
{
	XBee* xbee;
	if ((xbee = _GetXBeePointer(huart, NULL)) != NULL)
	{
		if (xbee->State == XBEE_STATE_BUSY)
		{
			xbee->State = XBEE_STATE_READY;
		}
	}
}

//Is a HAL timer callback registered function.
//HAL_TIM_RegisterCallback
//HAL_TIM_PERIOD_ELAPSED_CB_ID
void XBee_TimerTick(TIM_HandleTypeDef* htim)
{
	XBee* xbee;
	if ((xbee = _GetXBeePointer(NULL, htim)) != NULL)
	{
		if (xbee->State == XBEE_STATE_INIT)
		{
			//Send the '+++' command every X seconds until a OK response is received
			if (xbee->InitState == INIT_START)
			{
				xbee->Timer.Counter ++;
				if (xbee->Timer.Counter == xbee->Timer.InitTimeout)
				{
					xbee->Timer.Counter = 0;
					HAL_UART_Transmit_IT(xbee->UART, XBEE_INIT_COMMANDS[INIT_START], 3);
				}
			}
			//Send the 'AI' command every 0.1 second until a 0 or 22 response is received
			else if (xbee->InitState == INIT_AI)
			{
				HAL_TIM_Base_Stop_IT(xbee->Timer.Timer);
				HAL_UART_Transmit_IT(xbee->UART, XBEE_INIT_COMMANDS[xbee->InitState], 5);
			}
		}
		else
		{
			HAL_TIM_Base_Stop_IT(xbee->Timer.Timer);
		}
	}
}

void XBee_Init(XBee *xbee, UART_HandleTypeDef *huart, TIM_HandleTypeDef *htim, char *PanID)
{
	//Registers the HAL callback events to the associated functions
	HAL_UART_RegisterCallback(huart, HAL_UART_TX_COMPLETE_CB_ID, XBee_TransmittedData);
	HAL_UART_RegisterRxEventCallback(huart, XBee_ReceivedData);
	HAL_TIM_RegisterCallback(htim, HAL_TIM_PERIOD_ELAPSED_CB_ID, XBee_TimerTick);

	XBeeTimer timer;
	timer.InitTimeout = 50; //50 * 100 = 5000 milliseconds
	timer.Timer = htim;
	xbee->Timer = timer;
	xbee->UART = huart;
	xbee->PAN_ID = PanID;
	xbee->FrameID = 1;
	xbee->State = XBEE_STATE_INIT;
	xbee->InitState = INIT_START;

	//Sets the ID command parameter to the PanID - Used in the configuration process
	strcat((char*)XBEE_CONFIG_COMMANDS[0], xbee->PAN_ID);
	strcat((char*)XBEE_CONFIG_COMMANDS[0], "\r");

	//Adds the XBee type structure to the array and increments the counter.
	//Used for getting the correct XBee pointer in HAL callback events
	XBeePointerArray[PointerArrayIndex] = xbee;
	PointerArrayIndex ++;

	//TODO:Check/Configure timer for correct interval
	//HAL_RCC_GetHCLKFreq();

	HAL_UARTEx_ReceiveToIdle_IT(xbee->UART, xbee->RxBuffer, XBEE_BUFFER_SIZE);
	HAL_UART_Transmit_IT(xbee->UART, XBEE_INIT_COMMANDS[INIT_START], 3); //Sends "+++" to the XBee device
	HAL_TIM_Base_Start_IT(htim);
}

//Starts the configuration process by setting the state and sending the initial AT command
void XBee_Configure(XBee *xbee)
{
	xbee->State = XBEE_STATE_CONFIG;
	xbee->ConfigState = 0;
	HAL_UART_Transmit_IT(xbee->UART, XBEE_CONFIG_COMMANDS[0], strlen((char*)XBEE_CONFIG_COMMANDS[0]));
}

//Returns a new frame ID
uint8_t XBee_GetFrameID(XBee *xbee)
{
	uint8_t _FrameID = xbee->FrameID;
	if (_FrameID == 255)
	{
		xbee->FrameID = 1;
	}
	else
	{
		xbee->FrameID ++;
	}
	return _FrameID;
}

//Transmits a AT command frame to a local XBee module
void XBee_TransmitATCommand(XBee *xbee, char Payload[])
{
	ATCommand PacketStruct;
	PacketStruct.FrameID = XBee_GetFrameID(xbee);
	PacketStruct.FrameType = AT_COMMAND;
	strcpy((char*)PacketStruct.Data, Payload);
	uint8_t DataLength = XBee_EncodeData((uint8_t*)&PacketStruct, sizeof(PacketStruct), strlen(Payload), xbee->FxBuffer);
	HAL_UART_Transmit_IT(xbee->UART, xbee->FxBuffer, DataLength);
}

//Transmits arbitrary data to the designated remote XBee module
void XBee_TransmitData(XBee *xbee, char Payload[], const uint8_t Address16[], const uint8_t Address64[])
{
	if (xbee->State == XBEE_STATE_READY)
	{
		xbee->State = XBEE_STATE_BUSY;
		TransmitRequest PacketStruct;
		PacketStruct.FrameID = XBee_GetFrameID(xbee); //Sets the FrameID structure member to a new frame ID
		PacketStruct.BroadcatRadius = 0; //Sets the BroadcastRadius structure member to 0 - Should always be 0
		PacketStruct.Options = 0; //Sets the Options structure member to 0 - Is used for encryption
		PacketStruct.FrameType = TRANSMIT_REQUEST; //Sets the FrameType structure member to 0x10
		memcpy(PacketStruct.Address64, Address64, 8); //Copies the 64 bit address parameter into the correct structure member
		strcpy((char*)PacketStruct.Address16, (char*)Address16); //Copies the 16 bit address parameter into the correct structure member
		strcpy((char*)PacketStruct.Data, Payload); //Copies the arbitrary payload data into the correct structure member
												   //NOTE: Maybe a length parameter should be used to determine how many bytes to copy and encode

		//Encodes the PacketStruct into XBee's FX buffer and transmits it.
		//NOTE: (uint8_t*)&PacketStruct is a pointer to the PacketStruct variable, and because all members of the structure
		//are of the same type it can be iterated through as an array.
		uint8_t DataLength = XBee_EncodeData((uint8_t*)&PacketStruct, sizeof(PacketStruct), strlen(Payload), xbee->FxBuffer);
		HAL_UART_Transmit_IT(xbee->UART, xbee->FxBuffer, DataLength);
	}
}

//Decodes frame data and runs the associated callback function
void XBee_DecodeData(uint8_t Data[], uint8_t Size, XBee *xbee)
{
	if (Data[0] == RECEIVE_PACKET)
	{
		ReceivePacket PacketStruct;
		PacketStruct.DataLength = Size-RECEIVE_PACKET_DATA_OFFSET;
		memcpy(PacketStruct.Data, Data+RECEIVE_PACKET_DATA_OFFSET, PacketStruct.DataLength);
		memcpy((uint8_t*)&PacketStruct, Data, RECEIVE_PACKET_DATA_OFFSET);
		XBee_DataReceivedCallback(xbee, PacketStruct);
	}
	if (Data[0] == TRANSMIT_STATUS)
	{
		TransmitStatus PacketStruct;
		memcpy((uint8_t*)&PacketStruct, Data, sizeof PacketStruct);
		XBee_TransmitStatusCallback(xbee, PacketStruct);
	}
	if (Data[0] == MODEM_STATUS)
	{
		ModemStatus PacketStruct;
		memcpy((uint8_t*)&PacketStruct, Data, sizeof PacketStruct);
		XBee_ModemStatusCallback(xbee, PacketStruct);
	}
}

//Encodes arbitrary data and calculates the checksum
uint8_t XBee_EncodeData(uint8_t* FrameStruct, uint8_t FrameSize, uint8_t DataSize, uint8_t* Result)
{
	Result[0] = XBEE_START_DELIMITER;
	Result[XBEE_MSB_INDEX] = 0;
	Result[XBEE_LSB_INDEX] = FrameSize - XBEE_FRAME_DATA_SIZE + DataSize;
	int Checksum = 0;

	for (int i = 0; i < Result[XBEE_LSB_INDEX]; i++)
	{
		Checksum += FrameStruct[i];
		Result[i + XBEE_FRAME_OFFSET] = FrameStruct[i];
	}
	uint8_t TotalLength = Result[XBEE_LSB_INDEX] + XBEE_FRAME_OFFSET;
	Result[TotalLength] = 0xFF - (Checksum & 0xFF);
	return TotalLength + 1;
}

__weak void XBee_InitCallback(XBee *XBeeStruct)
{
	UNUSED(XBeeStruct);
}

__weak void XBee_DataReceivedCallback(XBee *XBeeStruct, ReceivePacket Packet)
{
	UNUSED(XBeeStruct);
	UNUSED(Packet);
}

__weak void XBee_TransmitStatusCallback(XBee *XBeeStruct, TransmitStatus Packet)
{
	UNUSED(XBeeStruct);
	UNUSED(Packet);
}

__weak void XBee_ModemStatusCallback(XBee *XBeeStruct, ModemStatus Packet)
{
	UNUSED(XBeeStruct);
	UNUSED(Packet);
}
