/* Zigbee_protocol.h */
#ifndef ZIGBEE_PROTOCOL_HEADER
#define ZIGBEE_PROTOCOL_HEADER

#include "stm32g4xx_hal.h"

#define XBEE_BUFFER_SIZE 0xFF //Maximum length of a Zigbee frame is 0xFF(256) bytes
#define XBEE_FRAME_DATA_SIZE 64

//Frame Types
#define TRANSMIT_REQUEST 0x10
#define AT_COMMAND 0x08
#define RECEIVE_PACKET 0x90
#define TRANSMIT_STATUS 0x8B
#define MODEM_STATUS 0x8A

#define XBEE_START_DELIMITER 0x7E
#define XBEE_FRAME_OFFSET 3 //Used to exclude the start delimiter, MSB and LSB in aspects of the encoding/decoding process
#define RECEIVE_PACKET_DATA_OFFSET 12 //How many bytes into a ReceivePacket before the arbitrary data field starts

static const uint8_t XBEE_COORDINATOR_ADDRESS[8];
static const uint8_t XBEE_UNKNOWN_16ADDRESS[2] = {0xFF, 0xFE};

#define XBEE_MSB_INDEX 1 //Index of Most Significant Byte in a Zigbee frame
#define XBEE_LSB_INDEX 2 //Index of Least Significant Byte in a Zigbee frame
#define XBEE_FRAME_TYPE_INDEX 3 //Index of the frame type in a Zigbee frame

typedef struct _ReceivePacket {
	uint8_t FrameType;
	uint8_t Address64[8];
	uint8_t Address16[2];
	uint8_t Options;
	uint8_t Data[XBEE_FRAME_DATA_SIZE];
	uint8_t DataLength; //Is calculated when decoding. Can be used to easily extract the Data member of the struct
} ReceivePacket;

typedef struct _TransmitRequest {
	uint8_t FrameType;
	uint8_t FrameID;
	uint8_t Address64[8];
	uint8_t Address16[2];
	uint8_t BroadcatRadius;
	uint8_t Options;
	uint8_t Data[XBEE_FRAME_DATA_SIZE];
} TransmitRequest;

enum DeliveryStatus {
	Success,
	MAC_ACK_Failure,
	CCA_Failure,
	Invalid_destination_endpoint = 0x15,
	Network_ACK_Failure = 0x21,
	Not_Joined_Network,
	Self_addressed,
	Address_Not_Found,
	Route_Not_Found,
	Neighbor_did_not_relay,
	Invalid_binding_table_index = 0x2B,
	Resource_error,
	Attempted_broadcast_with_APS_transmission,
	Attempted_unicast_with_APS_transmission_and_EE0,
	Resource_error2 = 0x32,
	Data_payload_too_large = 0x74,
	Indirect_message_unrequested
};

enum DiscoveryStatus {
	No_Discovery_Overhead,
	Address_Discovery,
	Route_Discovery,
	Address_and_Route,
	Extended_Timeout_Discovery = 0x40
};

typedef struct _TransmitStatus {
	uint8_t FrameType;
	uint8_t FrameID;
	uint8_t Address16[2];
	uint8_t Retries;
	enum DeliveryStatus DeliveryStatus;
	enum DiscoveryStatus DiscoveryStatus;
} TransmitStatus;

enum ModemStatus {
	Hardware_reset,
	Watchdog_timer_reset,
	Joined_network,
	Disassociated,
	Coordinator_started = 6,
	Network_security_key_was_updated,
	Voltage_supply_limit_exceeded = 0x0D,
	Modem_configuration_changed_while_join_in_progress = 0x11,
	Ember_ZigBee_stack_error = 0x80, //0x80 or above
};

typedef struct _ModemStatus {
	uint8_t FrameType;
	enum ModemStatus Status;
} ModemStatus;

typedef struct _ATCommand {
	uint8_t FrameType;
	uint8_t FrameID;
	uint8_t Data[XBEE_FRAME_DATA_SIZE]; //AT Command and optional parameter
} ATCommand;


typedef struct _XBeeTimer {
	int Counter;
	int InitTimeout;
	TIM_HandleTypeDef *Timer;
} XBeeTimer;

enum XBee_InitState	{INIT_START, INIT_AP, INIT_AI, INIT_ACK, INIT_DONE};

typedef struct _XBeeDevice {
	char* PAN_ID;
	int State;
	enum XBee_InitState InitState;
	int ConfigState; //Is only used for iterating the AT commands when configuring
	UART_HandleTypeDef *UART;
	XBeeTimer Timer;
	uint8_t FrameID;
	uint8_t RxBuffer[XBEE_BUFFER_SIZE];
	uint8_t FxBuffer[XBEE_BUFFER_SIZE];
} XBee;

#define XBEE_STATE_INIT 	1
#define XBEE_STATE_CONFIG 	2
#define XBEE_STATE_BUSY 	128
#define XBEE_STATE_READY 	256

//Init ATC Mode response definitions
#define INIT_RESPONSE_OK 	"OK\r"
#define INIT_RESPONSE_0		"0\r"
#define INIT_RESPONSE_22	"22\r"

//Init ATC Mode commands definitions
static const uint8_t XBEE_EXIT_ATC_MODE[] = "ATCN\r";
static const uint8_t XBEE_INIT_COMMANDS[][5] = {"+++", "ATAP\r", "ATAI\r"};
static uint8_t XBEE_CONFIG_COMMANDS[][15] = {"ATID"/*Parameter is applied at init*/, "ATAP1\r", "ATWR\r", "ATAC\r"};
static const uint8_t XBEE_CONFIG_COMMANDS_SIZE = (sizeof(XBEE_CONFIG_COMMANDS)/sizeof(*XBEE_CONFIG_COMMANDS));

//Callback definitions
void XBee_InitCallback(XBee *XBeeStruct);
void XBee_DataReceivedCallback(XBee *XBeeStruct, ReceivePacket Packet);
void XBee_TransmitStatusCallback(XBee *XBeeStruct, TransmitStatus Packet);
void XBee_ModemStatusCallback(XBee *XBeeStruct, ModemStatus Packet);

//Registered HAL callback events functions
void XBee_TransmittedData(UART_HandleTypeDef *huart);
void XBee_ReceivedData(UART_HandleTypeDef *huart, uint16_t Size);
void XBee_TimerTick(TIM_HandleTypeDef *htim);

//Native function definitions
void XBee_Init(XBee *xbee, UART_HandleTypeDef *huart, TIM_HandleTypeDef *htim, char* PanID);
void XBee_Configure(XBee *xbee);
uint8_t XBee_EncodeData(uint8_t *FrameStruct, uint8_t FrameSize, uint8_t DataSize, uint8_t *Result);
void XBee_DecodeData(uint8_t *Data, uint8_t Size, XBee *xbee);
void XBee_TransmitData(XBee *xbee, char Payload[], const uint8_t Address16[], const uint8_t Address64[]);
void XBee_TransmitATCommand(XBee *xbee, char Payload[]);

#endif /* ZIGBEE_PROTOCOL_HEADER */
