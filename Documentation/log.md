# LOG
## 09/09/2022

zigbee modules revieved  
Overview of datasheets  
invited Ruslan to repository  
list for modules and controllers needed  
documentation is made using markdown  
Ruslan's project files added to repo
Tested XTCU with Parallax module and the Xbee s2c RF module

## 16/09/2022

Status meeting with Ilias and Alan  
Furps and MoSCoW  
STM32 recieved  
STM32 setup with push button  
STM32 can send data via Xbee to RPi using UART  
Can not receive  
RPi can send and receive  

## 21/09/2022
STM32 with Xbee router can trasmit and receive using UART DMA.

## 23/09/2022
STM32 can send a packet with the value of '1' to the coordinator.  
The coordinator sends it to another STM32 (via a hardcoded MAC address).  
The STM32 toggles its onboard LED when recieving a packet with the value of '1'.

### Webserver
Work on a Firebase script for testing.   
Frontend data sync implemented.  
Dependencies added.

## 30/09/2022
### STM32
Added functionality to encode and decode a transmit request packet.
## 05/09/2022
### STM32
Added functionality for automatic configuration of Xbee Module.
## Report
Project Exam part 1 report writing started.

## 07/09/2022
Project Exam part 1 report writing.

# Part 2
## 02/11/2022
Talked about how to improve project management based on part 1 of the project  
Revised the FURPS/MoSCoW table
Created a backlog (Sprint goals, issues, tasks) based on the requirements table  
Made a schedule/timeline based on the backlog and delivery date
## 04/11/2022
Distributed tasks between members.
### STM32
Researched and worked on the BME280 and STM32  
### Webserver
Receiving network device list and sending it to frontend  
### Coordinator - Python
Merged all test script's functionality into one script, except  
Firebase DB and Xbee data communication
Capability to detect changes in thenetwork and sending events to the backend  
Tested and researched async functionality and code cleanup

## 05/11/2022
Organized Repository and Documentation 
### Coordinator - Python
Rewrote the network discovery process to use callback functions

## 09/11/2022
STM32 reading from BME280: working  
STM32 sending the data to coordinator  
raspberry pi sending the bme280 data to firebase  

power consumption analysis started 
