Update to the Zigbeefirmware

# XCTU
## Coordinator
- Selects a channel and PAN ID (both 64-bit and 16-bit) to start the network
- Can allow routers and end devices to join the network
- Can assist in routing data
- Cannot sleep--should be mains powered.

### Settings
Default except:  
PAN ID 902  
DL Destination Address Low - used for receiving address FFFF  
CE Coordinator Enable - Set to Coordinator [1]  
Node identifier Coordinator  
API enable 1  

## End Point / Router
- Must join a ZigBee PAN before it can transmit or receive data
- Cannot allow devices to join the network
- Must always transmit and receive RF data through its parent. Cannot route data
- Can enter low power modes to conserve power and can be battery-powered  

### Settings
Channel verification enabled  
ID PAN ID - Same as Coordinator  
CE Coordinator Enable - Set to End Point [0]  
Node identifier Router  

## Router
- Must join a ZigBee PAN before it can transmit, receive, or route data
- After joining, can allow routers and end devices to join the network
- After joining, can assist in routing data
- Cannot sleep--should be mains powered  

# RPi
Create virtual environment
install pyserial (pip3 install pyserial)
Check if the XBee device shows up (lsusb)