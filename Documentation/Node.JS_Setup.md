# Webapp Setup Guide
## Prerequisites
 - Node.JS
 - NPM
 - Cloned/Pulled this project  

## Instructions
1. Navigate to *iotnet/Webapp* in a CLI
2. Install Dependencies with *npm install #*
3. Start the app with *npm start*  