# Requirements
 **FURPS Requirements** | **M** | **S** | **C** | **W**
 :---:|:---:|:---:|:---:|:---:
 **Functionality** | **Must** | **Should** | **Could** | **Won't**
| | 3 XBee modules | Battery powered | |
| | Input(Web UI) & output(Sensors) | Encryption | |
| | UART between STM32 & XBee | | |
**Usability** | **Must** | **Should** | **Could** | **Won't**
| | Web interface | | |
**Reliability** | **Must** | **Should** | **Could** | **Won't**
| | | Veroboard | PCB | 
**Performance** | **Must** | **Should** | **Could** | **Won't**
| | | 3+ routers | |
**Supportability** | **Must** | **Should** | **Could** | **Won't**
| | STM32 using cubeIDE | | |

MOSCOW
    Must have: 
        A mesh network between 3 Xbee modules   
        2 routers and 1 coordinator
        Web interface
        An input on one router and an output on the other router
        STM32
            - developed using stm32 cubeIDE
        Communication between STM32 and Xbee using UART
    Should have:
        3+ routers and one coordinator
        battery power supply
        Veroboard
        Schematic
        Data encryption
    Could have:
        PCB
    Won't have:

FuRPs:
    Functionality:
        A mesh network between 3 Xbee modules  
        An input on one router and an output on the other router
        battery power supply
        Data encryption
    Usability:
        Web interface

    Reliability:
        PCB
        Veroboard
    Performance:
        3+ routers and one coordinator
    Supportability:
        STM32
            - developed using stm32 cubeIDE
        Schematic

