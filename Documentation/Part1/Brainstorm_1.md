# Ultra fast and short BS

* PCB with sockets for Xbee s2c module, STM32 nucleo, remaining STM pins and a power regulator
* External antenna for the Xbee module
* Compact enclousure for the complete setup
* Calculate powerdraw and runtime for different powersource options (Prop. lipo, but how many mAh?)
* Re-chargable units, or re-chargable replacable batteries
* Charge station
* Thermal calculations.....hope somebody took notes.....
* Battery charge state information
* Firmware choice (pros and cons)
* Logging of node temperature
* Method for storing data
* Sensor options
* Actuator options
* Replacable sensors
* Replacable actuators
* Powerdraw with different sensor/actuator options