# Milestones, issues and tasks

1. Milestone
    - Issues 1
        - Task 1
        - task 2
    - Issue 2
        - Task 1
## Milestones
1. From sensor to website
    - Get readings from BME280
        - BME280 sensor lib for STM32
    - Upload data to FirebaseDB
    - Display Readings on Webpage
2. Battery Power
    - Power consumption analysis
    - Find suitable power supply 
3. Expanded web functionality
    - List of web devices
    - graphs over temperature readings
        - Get data from firebaseDB
4. Security
    - Encryption
        - HTTPS
        - I/O Socket
        - XBee encryption?
5. Create PCB
    - Design PCB
        - Create PCB layout in Altium
        - Find components for PCB
    - Get PCB manufactured
        - Find manufacture
        - Order PCB

