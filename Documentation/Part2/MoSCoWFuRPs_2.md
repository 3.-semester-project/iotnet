# Requirements
 **FURPS Requirements** | **M** | **S** | **C** | **W**
 :---:|:---:|:---:|:---:|:---:
 **Functionality** | **Must** | **Should** | **Could** | **Won't**
| | 3 XBee modules |  | |
| | Input(Web UI) | | |
| | Get and show data from sensor | BME280 temperature| Multiple sensors |
| | UART between STM32 & XBee | | |
**Usability** | **Must** | **Should** | **Could** | **Won't**
| | Controle devices from webinterface | Battery power | |Device configuration from web interface|
| | XBee Auto configuration | | |
**Reliability** | **Must** | **Should** | **Could** | **Won't**
| | | | PCB |
|||Encryption | 
**Performance** | **Must** | **Should** | **Could** | **Won't**
| | | 3+ routers | |
**Supportability** | **Must** | **Should** | **Could** | **Won't**
| | STM32 using cubeIDE | | |


