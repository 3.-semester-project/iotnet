# Guides, Manuals and References

## XBee
General XBee Documentation and XBee datapacket documentation  
[API Frame Structure](https://www.digi.com/resources/documentation/Digidocs/90001942-13/Default.htm "API Frame Structure")

Thorough and comprehensive documentation on every aspect of the S2C module  
[XBee S2C Zigbee RF Module Manual](https://robokits.download/downloads/Xbee_Manual.pdf "XBee S2C Zigbee RF Module Manual")

A guide for setting up XBee devices used in this project with XCTU  
[Xbee Setup Guide](https://gitlab.com/3.-semester-project/iotnet/-/blob/main/Documentation/Xbee_Setup.md)

------------
## STM32
User manual and API references for the HAL and LL drivers  
[STM32 Drivers](https://www.st.com/resource/en/user_manual/dm00105879-description-of-stm32f4-hal-and-ll-drivers-stmicroelectronics.pdf)

Guides for using UART  
[UART Receive](https://controllerstech.com/uart-receive-in-stm32/ "UART Receive")  
[UART Transmit](https://controllerstech.com/uart-transmit-in-stm32/ "UART Transmit")

------------
## Node.JS
[Node.JS Starting Guide](https://www.c-sharpcorner.com/article/building-web-application-using-node-js/)  

Guide for setting up the Node.JS web application from this project  
[Webapp Setup Guide](https://gitlab.com/3.-semester-project/iotnet/-/blob/main/Documentation/Node.JS_Setup.md)

### Packages/Dependencies
https://socket.io  
https://expressjs.com  
[Firebase Admin SDK](https://firebase.google.com/docs/admin/setup#node.js)

------------
## Python
[Asynchronous I/O Library](https://docs.python.org/3/library/asyncio.html)  
[Firebase Admin SDK](https://firebase.google.com/docs/admin/setup#python)  
[Socket.IO Library](https://python-socketio.readthedocs.io/en/latest/api.html)  
[Xbee Library](https://xbplib.readthedocs.io/en/latest/index.html)

------------
## Other
https://firebase.google.com/docs/reference/admin