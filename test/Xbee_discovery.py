from digi.xbee.devices import XBeeDevice
import random
import time

PORT = "COM4"
BAUD_RATE = 9600
COORDINATOR_ID = "IoT_Coordinator"

device = XBeeDevice(PORT, BAUD_RATE)

try:
    device.open()
    xbee_network = device.get_network()
    xbee_network.set_discovery_timeout(5)
    coordinator = xbee_network.discover_device(COORDINATOR_ID)
    xbee_network.clear()
    while True:
        time.sleep(5)
        data = random.randrange(10,100)
        device.send_data(coordinator, str(data))
        print("send:", data)

finally:
    if device is not None and device.is_open():
        device.close()