/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define START_DELIMITER 0x7E
#define RECEIVE_PACKET 0x90//Zigbee Frame Type
#define TRANSMIT_REQUEST 0x10//Zigbee Frame Type
#define LSB_FRAME_INDEX 2//Index of Least Significant Byte in a Zigbee Frame
#define FRAMETYPE_FRAME_INDEX 3//Index of the frame type in a Zigbee Frame
#define BUFFER_SIZE 1//If buffer size exceeds the remaining data
					 //the UART handler won't pass it into the buffer
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
UART_HandleTypeDef huart1;
DMA_HandleTypeDef hdma_usart1_rx;
DMA_HandleTypeDef hdma_usart1_tx;

/* USER CODE BEGIN PV */
int MAC_Frame_Index_Start = 4;//Start index of source MAC - Frame type specific
int Message_Frame_Index_Start = 15;//Start index of message - Frame type specific
int MAC_Frame_Index_End = 11;//End index of source MAC - Frame type specific
int Message_Frame_Index_End;//End index of message - Frame type specific
uint8_t RX_Buffer[BUFFER_SIZE];//UART RX Buffer - If buffer size exceeds the remaining data
							   //the UART handler won't pass it into the buffer

int Frame_Length = 0;
int Total_Frame_Length = -1;//Must be < 0 or > LSB_FRAME_INDEX
int Index_Count, MAC_Count, Message_Count = 0;

//Variables for different frame structures - Some are frame type specific
uint8_t Checksum, FrameType;
uint8_t MAC_Address[8];
char Message_Data[20];
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_USART1_UART_Init(void);
/* USER CODE BEGIN PFP */

//Is called when UART RX buffer is filled
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	if (huart->Instance == huart1.Instance)//Only allows the UART handler connected to the XBee
	{
		//Start of frame indicator
		if (RX_Buffer[0] == START_DELIMITER)
		{
			HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_8);
			//Do stuff
		}

		//Iterates the bytes in the UART RX Buffer
		for (int i = 0; i < BUFFER_SIZE; i++)
		{
			//Looks for the Least Significant Byte - The length of the Zigbee frame
			if (Index_Count == LSB_FRAME_INDEX)
			{
				Frame_Length = RX_Buffer[i];//Saves the LSB byte
				Total_Frame_Length = Frame_Length + 3;//Adds 3 because the start delimiter,
													  //MSB and LSB is excluded from the LSB value
				Message_Frame_Index_End = Total_Frame_Length - 1;//Sets the message end index - Frame type specific
			}
			//Looks for the frame type. Is not used for anything ATM.
			else if (Index_Count == FRAMETYPE_FRAME_INDEX)
			{
				FrameType = RX_Buffer[i];//Sets the frame type variable
				if (FrameType == RECEIVE_PACKET)//Checks for a specific frame type
				{
					//Do stuff in relation to the specific frame type.
					//Can be used to set frame type specific indexes in relation to the data packet,
					//so the specific frame can be decoded correctly.
				}

			}
			//Checks if the byte is a part of the source MAC address - Frame type specific
			else if (Index_Count >= MAC_Frame_Index_Start && Index_Count <= MAC_Frame_Index_End)
			{
				MAC_Address[MAC_Count] = RX_Buffer[i];//Saves the byte into a variable
				MAC_Count ++;//Adds 1 so the next byte is saved correctly into the array
			}
			//Checks if the byte is a part of the message/data structure - Frame type specific
			else if (Index_Count >= Message_Frame_Index_Start && Index_Count <= Message_Frame_Index_End)
			{
				Message_Data[Message_Count] = RX_Buffer[i];//Saves the byte into a variable
				Message_Count ++;//Adds 1 so the next byte is saved correctly into the array
			}
			//End of frame indication
			if (Index_Count == Total_Frame_Length)
			{
				Checksum = RX_Buffer[i];//Saves the checksum. Not necessary as the XBee checks it
				//Resets necessary variables for next packet
				Index_Count = 0;
				MAC_Count = 0;
				Message_Count = 0;
				Frame_Length = 0;
				HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_8);
				break;//No reason to break when BUFFER_SIZE is 1
			}
			Index_Count++;//Counts the amount of received bytes.
		}
		HAL_UART_Receive_DMA(&huart1, RX_Buffer, BUFFER_SIZE);
	}
}

void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
	HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_8);
	HAL_UART_AbortTransmit_IT(&huart1);
}

uint16_t str2hexsum(char array[])
{
	int len = strlen(array);
	uint16_t sum = 0;
	for (int i = 0; i < len; i++)
	{
		sum += array[i];
	}
	return sum;
}
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */


/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_USART1_UART_Init();
  /* USER CODE BEGIN 2 */
  HAL_UART_Receive_DMA(&huart1, RX_Buffer, BUFFER_SIZE);//Set the DMA mode to circular in the Device Configuration Tool
  ////////////////////////////////////////////////////////
  ///////////////////\/Transmitting WIP\//////////////////
  uint8_t MSB = 0x00;
  uint8_t FrameID = 0x01;
  //uint8_t MAC[8] = { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,};
  uint8_t Address_16bit[2] = { 0xFF, 0xFE };
  //uint8_t Broadcast_Radius = 0x00;
  //uint8_t Options = 0x00;
  char msg[] = "Piss on a cat";
  uint16_t hexsum = str2hexsum(msg);
  int msg_len = strlen(msg);
  uint8_t LSB = (1 + 1 + msg_len + 8 + 2 + 1 + 1);
  unsigned char _cs = ((TRANSMIT_REQUEST + FrameID + Address_16bit[0] + Address_16bit[1] + hexsum) & 0xff);
  unsigned char _Checksum = 0xFF - _cs;
  uint8_t TX_Buffer[40] = { START_DELIMITER, MSB, LSB, TRANSMIT_REQUEST, FrameID };
  int msg_count = 0;
  for (int i = 0; i < 40;i++)
  {
	  if (i == 13)
	  {
		  TX_Buffer[i] = 0xFF;
	  }
	  if (i == 14)
	  {
		  TX_Buffer[i] = 0xFE;
	  }
	  if (i > 16 && i <= LSB + 2)
	  {
		  TX_Buffer[i] = msg[msg_count];
		  msg_count ++;
	  }
	  if (i == LSB + 3)
	  {
		  TX_Buffer[i] = _Checksum;
		  break;
	  }
  }
  HAL_UART_Transmit_DMA(&huart1, TX_Buffer, sizeof (TX_Buffer));
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
	  HAL_Delay(5000);
	  HAL_UART_Transmit_DMA(&huart1, TX_Buffer, sizeof (TX_Buffer));
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1_BOOST);

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = RCC_PLLM_DIV4;
  RCC_OscInitStruct.PLL.PLLN = 85;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 9600;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart1.Init.ClockPrescaler = UART_PRESCALER_DIV1;
  huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_SetTxFifoThreshold(&huart1, UART_TXFIFO_THRESHOLD_1_8) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_SetRxFifoThreshold(&huart1, UART_RXFIFO_THRESHOLD_1_8) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_EnableFifoMode(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void)
{

  /* DMA controller clock enable */
  __HAL_RCC_DMAMUX1_CLK_ENABLE();
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Channel1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel1_IRQn);
  /* DMA1_Channel2_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel2_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel2_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : LD2_Pin */
  GPIO_InitStruct.Pin = LD2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LD2_GPIO_Port, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
