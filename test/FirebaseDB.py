from digi.xbee.devices import XBeeDevice
import firebase_admin
from firebase_admin import db

cred_object = firebase_admin.credentials.Certificate('firebase-auth.json')
default_app = firebase_admin.initialize_app(cred_object, {'databaseURL':'https://fir-project-dd6e1-default-rtdb.europe-west1.firebasedatabase.app'})
ref = db.reference("/")
ref.set({"Test_Data": -1})

PORT = "/dev/ttyUSB0"
BAUD_RATE = 9600

device = XBeeDevice(PORT, BAUD_RATE)

try:
    device.open()
    #device.send_data_broadcast("cur")
    ref = db.reference("/Test_Data")
#     xbee_network = device.get_network()
#     xbee_network.set_discovery_timeout(5)
#     xbee_network.clear()
    
    def data_receive_callback(xbee_message):
        _data = xbee_message.data.decode()
        print("From %s >> %s" % (xbee_message.remote_device.get_64bit_addr(),
                                 _data))
        ref.push({"Data": _data})

    device.add_data_received_callback(data_receive_callback)
    
    print("Waiting for data...\n")
    input()

finally:
    if device is not None and device.is_open():
        device.close()
