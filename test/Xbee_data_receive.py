from digi.xbee.devices import XBeeDevice, RemoteXBeeDevice, XBee64BitAddress
from time import sleep
# Instantiate a local XBee node.
xbee = XBeeDevice('/dev/ttyUSB0', 9600)
xbee.open()

# Define the callback.
def my_data_received_callback(xbee_message):
	address = xbee_message.remote_device.get_64bit_addr()
	data = xbee_message.data.decode("utf8")
	print("Received data from %s: %s" % (address, data))

# Add the callback.
xbee.add_data_received_callback(my_data_received_callback)
remote = RemoteXBeeDevice(xbee, XBee64BitAddress.from_hex_string("0013A20041B8ABDA"))

data=b'0'

while True:
	try:
		sleep(3)
		xbee.send_data_broadcast("toggle_led_plz")
	except KeyboardInterrupt:
		xbee.close()
		break
