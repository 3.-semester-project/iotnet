import firebase_admin
from firebase_admin import db
import time
import random

cred_object = firebase_admin.credentials.Certificate('firebase-auth.json')
default_app = firebase_admin.initialize_app(cred_object, {'databaseURL':'https://fir-project-dd6e1-default-rtdb.europe-west1.firebasedatabase.app'})
ref = db.reference("/")
ref.set({"Test_Data": -1})
print('testing...')
ref = db.reference("/Test_Data")

while True:
    time.sleep(1)
    data = random.randrange(10,100)
    ref.push({"Data": data})
    print("send data to DB:", data)
