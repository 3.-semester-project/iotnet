import asyncio
import socketio
from digi.xbee.devices import XBeeDevice

PORT = "COM15"
BAUD_RATE = 9600
device = XBeeDevice(PORT, BAUD_RATE)
device.open()

sio = socketio.AsyncClient()

@sio.event
async def connect():
    print('connection established')
    await sio.emit('python', 'python connected')

@sio.event
async def toggle_led():
    print('Toggle LED received')
    device.send_data_broadcast("1")#Sends '1' to all 

@sio.event
async def disconnect():
    print('disconnected from server')

async def main():
    await sio.connect('http://localhost:8080')#Connects to the NodeJS IO Socket
    await sio.wait()

asyncio.run(main())