# IoTnet

## IoT based Network
Create a sensor based network user zigbee protocol. Create a WLAN network using iot technology and simple energy efficient hardware. User HTTPs protocol for representation of data and control of devices.
 
1. Create simple hardware layout, push button, LED and a IR sensor and interface to an Arduino.
2. Create a simple network using xbee. One server using a rpi and at least 3 Arduino as clients.
3. Create a simple website to interface and control the hardware.

### 2.
Create the xbee protocol on a Arduino using libraries. I advice that you actually do it yourselves. I will show you how. It will take more time, but it will help a lot in the next last step of the project.

### 3.
when you have a simple connection between Arduino and the RPI via xbee. Create a simple website to report the results. You can make a local host on the rpi so you do not need to pay money for an actual website. Alternative if you want to really learn, use firebase database to upload your data and a link it to a real website. This way you will develop hard skills needed in the market

### Next project phase
Migrate your C based software from the Arduino to an STM32. 


## useful links

[https://xbplib.readthedocs.io/en/latest/](https://xbplib.readthedocs.io/en/latest/)  
[https://pypi.org/project/digi-xbee/](https://pypi.org/project/digi-xbee/)  
with own web site with notes from teaching and start up guide plus some explanation  
[https://escoker.gitlab.io/iot-with-zigbee/](https://escoker.gitlab.io/iot-with-zigbee/)  
