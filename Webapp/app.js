//Network and Socket IO imports
const express = require('express');
const app = express();
const http = require('http').createServer(app);
const io = require('socket.io')(http);
app.use(express.static(__dirname + '/public'));

//Donno really know what this is for
const bodyParser = require('body-parser');
app.use(bodyParser.json());

//Firebase imports
const admin = require("firebase-admin");
const serviceAccount = require("./firebase-auth.json");

//Init the Firebase DB
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://fir-project-dd6e1-default-rtdb.europe-west1.firebasedatabase.app"
});

//Fires when the client goes to the root domain
app.get('/', (req, res) => {
	res.render('index.html');
});

//Database init and references
const db = admin.database();
const ref = db.ref("new");
const ref_legacy_entries = db.ref("legacy_entries");

//Firebase DB event - Fires once for every child/entry in the DB reference and also when a new one is added
//Read project documentation for information/links
ref.on('child_added', (snapshot) => {
	const data = snapshot.val();
	io.emit('new_child', data);
	console.log(data.MAC + ": " + data.value + data.unit);
});

//Firebase DB event - Fires once when a child/entry is removed from the DB reference
//Read project documentation for information/links
ref.on('child_removed', (snapshot) => {
  });
//--------------------------------------------------------------------

//HTTP event thingy - Not used - ajax testing
app.post('/append', (req, res) => {
	res.send({'response': 'good'});
});

let python_client //Stores the socket IO connection from the python script
let network_devices = [] //Stores the network's XBee routers
let legacy_entries = {} // Stores the legacy entries from the DB

//Fetches the legacy entries - Saved in same format as in DB
ref_legacy_entries.once('child_added', (snapshot) => {
	legacy_entries[snapshot.key] = snapshot.val()
});

//Default io socket connection event
io.on("connection", (socket) => {
	console.log('Client connected');
	socket.emit("init", network_devices, legacy_entries);

	//This socket event is called if the connected client is the python script.
	socket.on("python", (device_list) => {
		network_devices = device_list
		console.log("Python connected");
		console.log("Devices:" + network_devices);
		python_client = socket//Saves the python client to be able to reference it everywhere
	});

	//Occours when the python script discovers a new device
	socket.on("add_device", (device) => {
		network_devices.push(device);
		io.emit('device_added', device);
		console.log('hej med dig',device);
	});

	//Occours when the python script loses a known device
	socket.on("remove_device", (device) => {
		idx = network_devices.indexOf(device);
		network_devices.splice(idx, 1);
		io.emit('device_removed', device);
		console.log("mojn", device);
	});

	//Emits the toggle_led event in the python script
	socket.on("toggle_led", (device) => {
		python_client.emit('toggle_led', device)
	});

	//Firebase DB event - Fires ONCE for EVERY child/entry in the DB reference
	//Is used to send the DB data to the client/frontend on initial connection
	ref.once('child_added', (snapshot) => {
		const data = snapshot.val();
		socket.emit('init_child', data);//Sends the new DB entry to the frontend
	  });

	socket.on("disconnect", () => {
		console.log('Client disconnected');
		if (socket == python_client) {
			console.log("PYTHON SCRIPT DISCONNECTED");
			network_devices = []
			io.emit("python_disconnect");
		}
	});

	//Occours when a client submits a data query request
	/*Datasets format:
	{
		MAC: {
			UNIT: [VALUES...],
			...
		},
		...
	}*/
	socket.on("canvas",(selectors) => {
		var datasets = {};
		var startDate = selectors.startDate / 1000;
		var endDate = selectors.endDate / 1000;
		const queryRef = db.ref("legacy").orderByChild('timestamp').startAt(startDate).endAt(endDate);
		queryRef.get().then((snapshot) => {
			var latest = 0;
			var earliest = 0;
			snapshot.forEach((childSnapshot) => { //Iterates all the children of snapshot
				object = childSnapshot.val();
				if (selectors.MAC.indexOf(object.MAC) >= 0) { //Checks if the selectors contains the snapshot's mac
					if (selectors.unit.indexOf(object.unit) >= 0) { //Checks if the selectors contains the snapshot's unit
						if (earliest == 0) earliest = object.timestamp; //The first snapshot is always the earliest - Ordered by timestamp
						if (object.timestamp > latest) latest = object.timestamp; //Bad way of doing it - The last snapshot is always the last
						if (!datasets[object.MAC]) datasets[object.MAC] = {}; //Creates the mac key if it doesn't exists
						if (!datasets[object.MAC][object.unit]) datasets[object.MAC][object.unit] = []; //Creates the unit array if it doesn't exists
						if (object.unit == 'p') datasets[object.MAC][object.unit].push(object.value/100000); //Appends the value to the array in bar
						else datasets[object.MAC][object.unit].push(object.value); //Appends the value to the array
					}
				}
			});
			socket.emit("canvas_data", datasets, earliest, latest);
		})
	});
});

//Starts the http server
const PORT = 8080;
http.listen(PORT, () => {
	console.log(`App is listening on port ${PORT}!`);
});